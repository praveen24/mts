<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        table{
            width: 90%;
            border-collapse: collapse;
            margin: 0 auto;
        }
        table td,
        table th{
            padding: 10px;
            background-color: #fcfcfc;
            border: 1px solid #d8d8d8;
            text-align: left;
            word-break: break-all;
            word-wrap: break-word;
        }

        div.wrapper{
            background:#f5f5f5;
            border-radius: 5px;
            padding: 1%;
        }

        h2{
            text-transform: uppercase;
            text-decoration: underline;
        }

    </style>
</head>
<body>

    <div class="wrapper">
        <h2 style="text-align: center"> User Details </h2>
        <table>
            <tr>
                <th style="width:40%">Name</th>
                <td style="width:60%"><?= $name ?></td>
            </tr>
            <tr>
                <th style="width:40%">Email</th>
                <td style="width:60%"><?= $email ?></td>
            </tr>
            <tr>
                <th style="width:40%">Phone</th>
                <td style="width:60%"><?= $phone ?></td>
            </tr>
            <tr>
                <th style="width:40%">Message</th>
                <td style="width:60%"><?= $message ?></td>
            </tr>
            <tr>
                <td>
                    <span style="color: #aaa">This is system generated email. Do not respond to this email.</span>
                </td>
                <td style="text-align: right;">
                    copyright © <a href="<?= site_url() ?>">MTS</a>
                </td>
            </tr>
        </table>
    </div>
    
</body>
</html>