<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Vineeth N Krishnan">
        <meta name="description" content="Page description. No longer than 155 characters." />
        <!-- SEO Meta Start-->
        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="The Name or Title Here">
        <meta itemprop="description" content="This is the page description">
        <meta itemprop="image" content="http://www.example.com/image.jpg">
        <!-- Twitter Card data -->
        <meta name="twitter:card" content="product">
        <meta name="twitter:site" content="@way2vineeth">
        <meta name="twitter:title" content="Page Title">
        <meta name="twitter:description" content="Page description less than 200 characters">
        <meta name="twitter:creator" content="@way2vineeth">
        <meta name="twitter:image" content="http://www.example.com/image.jpg">
        <meta name="twitter:data1" content="">
        <meta name="twitter:label1" content="Price">
        <meta name="twitter:data2" content="Black">
        <meta name="twitter:label2" content="Color">
        <!-- Open Graph data -->
        <meta property="og:title" content="Title Here" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="http://www.example.com/" />
        <meta property="og:image" content="http://example.com/image.jpg" />
        <meta property="og:description" content="Description Here" />
        <meta property="og:site_name" content="Site Name, i.e. Moz" />
        <meta property="og:price:amount" content="15.00" />
        <meta property="og:price:currency" content="USD" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>News Portal</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/sweetalert.css') ?>">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
        <!-- Optional theme -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head><body>
        <div class="row">
<div class="col-md-6 col-md-offset-1">
    <div class="login-box">
        <div class="login-box-body">
            <h2 class="text-center"> Reset Password </h2>
            <div class="clearfix"></div><br>
            <?php $this->load->view('layouts/notification'); ?>
            <form novalidate="novalidate" id="da-login-form" method="post" action="<?= site_url('auth/reset_password/'.$act_code) ?>">
                <div class="form-group has-feedback col-md-10 col-md-offset-1 <?php echo (form_error('password') || isset($password_error)) ? 'text-danger' : ''; ?>">
                    <input name="password" placeholder="New Password" type="password" class="form-control"/>
                    <?php
                    if (form_error('password')) {
                        ?>
                        <span class="glyphicon glyphicon-lock form-control-feedback <?php echo (form_error('password') || isset($password_error)) ? 'text-danger' : ''; ?>"></span>
                        <span class="field_error"><?php echo form_error('password'); ?></span>
                        <?php
                    }
                    ?>
                </div>
                <div class="form-group has-feedback col-md-10 col-md-offset-1 <?php echo (form_error('confirm_password') || isset($confirm_password_error)) ? 'text-danger' : ''; ?>">
                    <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password"/>
                    <?php
                    if (form_error('confirm_password')) {
                        ?>
                        <span class="glyphicon glyphicon-lock form-control-feedback <?php echo (form_error('confirm_password') || isset($confirm_password_error)) ? 'text-danger' : ''; ?>"></span>
                        <span class="field_error"><?php echo form_error('confirm_password'); ?></span>
                        <?php
                    }
                    ?>
                </div>
                <input name="check" type="hidden"/>
                <div class="row">
                    <div class="col-md-11 text-center">
                        <button type="submit" class="btn btn-info" style="margin-right:13px;"><i class="glyphicon glyphicon-edit"></i> Submit</button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div><div class="clearfix"></div><br></div>

    </body>
</html>
