<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="<?= site_url('admin/home') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/sliders') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Sliders </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/services') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Services </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/products') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Products </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/about_us') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>About Us </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/image_gallery') ?>">
                        <i class="fa fa-image"></i>
                        <span>Image Gallery</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/video_gallery') ?>">
                        <i class="fa fa-image"></i>
                        <span>Video Gallery</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/menu') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Menu</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>