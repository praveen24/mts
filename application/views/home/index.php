<!-- Place somewhere in the <body> of your page -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 nopadding">
            <div id="homepage_slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="9000">
                <div class="carousel-inner">
                    <?php 
                    $i=0;
                    if(count($sliders)){
                        foreach ($sliders as $slider) { 
                        $i++;
                    ?>
                            <div class="carousel-item <?=$i==1?'active':''?>">
                                <?php if(!is_video($slider->image)){ ?>
                                <div style="background-image:url(<?= base_url('uploads/sliders/'.$slider->image) ?>);background-size: cover;background-position: center center;height: 750px;width: 100%" class="slider" />
                                    <div class="flex-caption">
                                        <p style="<?= $this->session->userdata('arabic') ? 'text-align: right;' : '' ?>"><?= $this->session->userdata('arabic') ? $slider->ar_title : $slider->en_title ?></p>
                                    </div>
                                </div>
                                <img src="<?= base_url('uploads/sliders/'.$slider->image) ?>" class="img-fluid mbl-slide">
                                <?php } 
                                else{ ?>
                                    
                                        <iframe id="player_1" src="<?= site_url('home/iframe?file='.base_url('uploads/sliders/'.$slider->image)) ?>" width="100%" frameborder="0" style="height: 750px"></iframe>
                                        <div class="flex-caption">
                                            <p style="<?= $this->session->userdata('arabic') ? 'text-align: right;' : '' ?>"><?= $this->session->userdata('arabic') ? $slider->ar_title : $slider->en_title ?></p>
                                        </div>
                                <?php } ?>
                            </div>
                        <?php }
                    } ?>
                    <a class="carousel-control left" href="#homepage_slider" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#homepage_slider" data-slide="next">&rsaquo;</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mts-spec">
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="spec">
                <h4 class="text-center"><?= $this->session->userdata('arabic') ? $menu->ar_spare : strtoupper($menu->en_spare) ?></h4>
                <img src="<?= base_url('assets/img/spare-parts.jpg') ?>" class="img-fluid mx-auto d-block">
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="spec">
                <h4 class="text-center"><?= $this->session->userdata('arabic') ? $menu->ar_services : strtoupper($menu->en_services) ?></h4>
                <img src="<?= base_url('assets/img/service.jpg') ?>" class="img-fluid mx-auto d-block">
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="spec">
                <h4 class="text-center"><?= $this->session->userdata('arabic') ? $menu->ar_support : strtoupper($menu->en_support) ?></h4>
                <img src="<?= base_url('assets/img/support.jpg') ?>" class="img-fluid mx-auto d-block">
            </div>
        </div>
    </div>
    <?php if($about_us){ ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="abt-mts">
                    <div class="row" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                            <h2><?= $this->session->userdata('arabic') ? $about_us->ar_title : $about_us->en_title ?></h2>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
                            <?= $this->session->userdata('arabic') ? $about_us->ar_desc : $about_us->en_desc ?> <a href="<?= site_url('about') ?>" style="<?= $this->session->userdata('arabic') ? 'float: left' : '' ?>"><?= $this->session->userdata('arabic') ? 'اقرأ أكثر' : 'READ MORE' ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>  
</div>
<div class="container">
    <div class="row service <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
        <div class="col-12">
            <div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
                <h2><?= $this->session->userdata('arabic') ? $menu->ar_our_services : strtoupper($menu->en_our_services) ?></h2>
            </div>
        </div>
        <?php if(count($highlight_services)){
            foreach ($highlight_services as $service) { ?>
                <div class="col-sm-12 col-md-4 col-lg-4 col-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
                    <div class="single-service">
                        <div class="service-img">
                            <img src="<?= base_url('uploads/services/'.$service->image) ?>" class="img-fluid">
                        </div>
                        <div class="service-content">
                            <h4 style="<?= $this->session->userdata('arabic') ? 'text-align: right;' : '' ?>"><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h4>
                            <?php $service->ar_desc = strip_tags($service->ar_desc);
                            $service->en_desc = strip_tags($service->en_desc); ?>
                            <p><?= $this->session->userdata('arabic') ? (mb_strlen($service->ar_desc) <= 100 ? $service->ar_desc : mb_substr($service->ar_desc, 0, 100).'...') : (strlen($service->en_desc) <= 100 ? $service->en_desc : substr($service->en_desc, 0, 100).'...') ?></p>
                            <div class="read_more" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
                                <a href="<?= site_url('service/'.$service->id) ?>"><?= $this->session->userdata('arabic') ? 'اقرأ أكثر' : 'READ MORE' ?> <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        }else{ ?>
            <div class="col-sm-12 col-md-4 col-lg-4 col-12">
                <div class="single-service">
                    <div class="service-img">
                        <img src="<?= base_url('assets/img/service1.jpg') ?>" class="img-fluid">
                    </div>
                    <div class="service-content">
                        <h4>Lorem Ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>
                        <div class="read_more">
                            <a href="#">READ MORE <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 col-12">
                <div class="single-service">
                    <div class="service-img">
                        <img src="<?= base_url('assets/img/service2.jpg') ?>" class="img-fluid">
                    </div>
                    <div class="service-content">
                        <h4>Lorem Ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>
                        <div class="read_more">
                            <a href="#">READ MORE <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 col-12">
                <div class="single-service">
                    <div class="service-img">
                        <img src="<?= base_url('assets/img/service3.jpg') ?>" class="img-fluid">
                    </div>
                    <div class="service-content">
                        <h4>Lorem Ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>
                        <div class="read_more">
                            <a href="#">READ MORE <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="clearfix"></div><br><br>
    <div class="row products <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>">
        <div class="col-12">
            <div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
                <h2><?= $this->session->userdata('arabic') ? $menu->ar_our_products : strtoupper($menu->en_our_products) ?></h2>
            </div>
            <div class="clearfix"></div>
            <section class="main">
                <ul class="ch-grid">
                    <?php if(count($highlight_products)){
                        foreach ($highlight_products as $product) { ?>
                            <li>
                                <div class="ch-item ch-img-3" style="background-image: url(<?= base_url('uploads/products/'.$product->image) ?>);">
                                    <div class="ch-info-wrap">
                                        <div class="ch-info">
                                            <div class="ch-info-front ch-img-3"></div>
                                            <div class="ch-info-back">
                                                <h3 style="<?= $this->session->userdata('arabic') ? 'text-align: right;direction: rtl' : '' ?>"><?= $this->session->userdata('arabic') ? $product->ar_title : $product->en_title ?></h3>
                                                <?php $product->ar_desc = strip_tags($product->ar_desc);
                                                $product->en_desc = strip_tags($product->en_desc); ?>
                                                <p style="<?= $this->session->userdata('arabic') ? 'text-align: right;direction: rtl' : '' ?>"><?= $this->session->userdata('arabic') ? (mb_strlen($product->ar_desc) <= 100 ? $product->ar_desc : mb_substr($product->ar_desc, 0, 100).'...') : (strlen($product->en_desc) <= 100 ? $product->en_desc : substr($product->en_desc, 0, 100).'...') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php }
                    } else{ ?>
                        <li>
                            <div class="ch-item ch-img-1" style="background-image: url(<?= base_url('assets/img/service1.jpg') ?>);">              
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-1"></div>
                                        <div class="ch-info-back">
                                            <h3>Service1</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-img-2" style="background-image: url(<?= base_url('assets/img/service2.jpg') ?>);">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-2"></div>
                                        <div class="ch-info-back">
                                            <h3>Service2</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-img-3" style="background-image: url(<?= base_url('assets/img/service3.jpg') ?>);">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-3"></div>
                                        <div class="ch-info-back">
                                            <h3>Service3</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-img-3" style="background-image: url(<?= base_url('assets/img/service1.jpg') ?>);">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-3"></div>
                                        <div class="ch-info-back">
                                            <h3>Service4</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </section>
        </div>
    </div>
</div>