<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  Gallery</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>
<div class="container">
	<div class="row gallery">
		<div class="col-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
				    <a class="nav-link active" id="image-tab" data-toggle="tab" href="#image" role="tab" aria-controls="Image" aria-selected="true">Image</a>
				</li>
				<li class="nav-item">
				    <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Video</a>
				</li>
			</ul>
			<div class="clearfix"></div><br>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="image" role="tabpanel" aria-labelledby="image-tab">	
					<div id="gallery">
						<?php foreach ($images as $image) { ?>
						  	<a href="<?= base_url('uploads/image/'.$image->image) ?>">
								<div class="col-lg-4 col-md-4 col-sm-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
									<img src="<?= base_url('uploads/image/'.$image->image) ?>" class="img-fluid" style="min-height: 250px;max-height: 300px;padding-bottom: 10px;">
									<div class="gallery-overlay">
										<h4 style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>"><?= $this->session->userdata('arabic') ? $image->ar_title : $image->en_title ?></h4>
										<i class="fa fa-search"></i>
									</div>
								</div>
							</a>
						<?php } ?>
					</div>
				</div>
				<div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
					<div id="video_gallery">
						<?php foreach ($gallery as $value) {
						$link = $value->link;
						$video_id = explode("?v=", $link);
						$video_id = $video_id[1];
						$img = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg'; ?>
						<a href="<?= $value->link ?>">
							<div class="col-lg-4 col-md-4 col-sm-12">
									<img src="<?= $img ?>" class="img-fluid">
									<div class="gallery-overlay">
										<h4>MTS</h4>
										<i class="fa fa-youtube-play"></i>
									</div>
							</div>
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br>