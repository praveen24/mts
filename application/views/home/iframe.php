<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			margin: 0;
			overflow: hidden;
		}
		video{
			position: relative;
		}
		@media(max-width: 1199px){
			video{
				width: auto;
				height: 750px;
			}
		}
		@media(max-width: 480px){
			.video{
				display: none !important;
			}
		}
	</style>
</head>
<body>
	<video muted autoplay loop id="video_player" style="height: 750px;"><source src="<?= $file ?>" type="video/mp4"></video>
</body>
</html>