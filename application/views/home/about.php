<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  About Us</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row about-content <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
		<div class="col-lg-6 col-md-6 col-sm-12 col-12">
			<img src="<?= $about_us?base_url('uploads/about_us/'.$about_us->image):base_url('assets/img/about.jpg') ?>" class="img-fluid">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-12">
			<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
				<?php if($about_us){ ?>
					<h2><?= $this->session->userdata('arabic') ? $about_us->ar_title : $about_us->en_title ?></h2>
				<?php }else{ ?>
					<h2><?= $this->session->userdata('arabic') ? 'مانكون' : 'WHAT WE ARE' ?></h2>
				<?php } ?>
				
			</div>
			<div class="clearfix"></div><br>
			<?php if($about_us){ ?>
				<?= $this->session->userdata('arabic') ? $about_us->ar_desc : $about_us->en_desc ?>
			<?php }else{ ?>
				<p><?= $this->session->userdata('arabic') ? '<p>متس تقف باعتبارها واحدة من تجار التجزئة الإنترنت الأكثر ثقة على الانترنت. التفاني، والالتزام بجودة الخدمات، ومستوى عال من الاحتراف والاعتمادية المنتج وأسعار تنافسية هي الأسباب التي تكمن وراء نجاحنا في صناعة السيارات. من خلال سنوات من توفير قطع غيار السيارات عالية الجودة والاكسسوارات على الصعيد الوطني. وقد تميزت شركتنا باعتبارها واحدة من قطع غيار السيارات الأكثر استقرارا على الانترنت. لدينا فريق من بروفيزيونالس في كثير من الأحيان تعمل على إعطائك خط كامل وأكثر تنافسية من قطع غيار السيارات وأفضل دعم العملاء كذلك.</p>' : 'MTS stands as one of the most trusted internet retailers online. Dedication, commitment to quality services, high level of professionalism and product dependability and competitive prices are reasons that underlie our success in the auto industry. Through the years of providing superior quality automotive parts and accessories nationwide. Our company has been distinguished as one of the most established auto parts retailers online. We have a team of profesionals frequently working to give you a complete and most competitive line of automotive parts and the best customer support as well.' ?></p>
			<?php } ?>
		</div>
	</div>
</div>