		<footer>
			<div class="big_footer_wrapper section_padding">
				<div class="container">
					<div class="row" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
						<div class="col-lg-3 col-md-3 col-sm-6 col-12">
							<img src="<?= base_url('assets/img/footer-logo.png') ?>" class="img-fluid mx-auto d-block" style="width: 200px"><br>
							<?php $about_us->ar_desc = strip_tags($about_us->ar_desc);
                            $about_us->en_desc = strip_tags($about_us->en_desc); ?>
							<p style="<?= $this->session->userdata('arabic') ? 'text-align: right;direction: rtl' : '' ?>"><?= $this->session->userdata('arabic') ? (mb_strlen($about_us->ar_desc) <= 120 ? $about_us->ar_desc : mb_substr($about_us->ar_desc, 0, 120).'...') : (strlen($about_us->en_desc) <= 120 ? $about_us->en_desc : substr($about_us->en_desc, 0, 120).'...') ?>
							</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-12">
							<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
								<h4><?= $this->session->userdata('arabic') ? 'روابط سريعة' : 'QUICK LINKS' ?></h4>
							</div>
							<div class="clearfix"></div>
							<div class="row" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
								<div class="col-sm-12 col-lg-6 col-md-6">
									<ul style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>;margin-bottom: 0;padding-bottom: 0;">
										<li><a href="<?= site_url() ?>"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a></li>
										<li><a href="<?= site_url('aboutus') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a></li>
										<li><a href="<?= site_url('products') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_products : $menu->en_products ?></a></li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12">
									<ul style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>;">
										<li><a href="<?= site_url('services') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_services : $menu->en_services ?></a></li>
										<li><a href="<?= site_url('gallery') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></a></li>
										<li><a href="<?= site_url('contact') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-12 <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>">
							<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
								<h4><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></h4>
							</div>
							<div class="clearfix"></div>
							<ul style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
								<li style="<?= $this->session->userdata('arabic') ? 'direction:rtl' : '' ?>">
									<span class="fa fa-paper-plane-o"></span>
									<div class="address_right">Saudi Arabia</div>
								</li>
								<li style="padding-top: 20px;<?= $this->session->userdata('arabic') ? 'direction:rtl' : '' ?>">
									<span class="fa fa-phone"></span>
									<div class="address_right"><?= $this->session->userdata('arabic') ? '٩٦٦١٢٦٢٣١٦٦٦+' : '+966126231666' ?></div>
								</li>
								<li style="padding-top: 20px;<?= $this->session->userdata('arabic') ? 'direction:rtl' : '' ?>">
									<span class="fa fa-envelope-o"></span>
									<div class="address_right">domain@gmail.com</div>
								</li>
							</ul>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-12">
							<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
								<h4><?= $this->session->userdata('arabic') ? 'ابق على اتصال' : 'STAY CONNECTED' ?></h4>
							</div>
							<div class="clearfix"></div>
							<ul class="social <?= $this->session->userdata('arabic') ? 'arabic_social' : '' ?>" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright-foeter">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 text-center">
							<p>Copyright &copy;2018 MTS. All Rights Reserved.</p>
						</div>
					</div>
				</div>
			</div>
		</footer>	
	
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/popper.min.js') ?>"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js"></script>
		<script type="text/javascript" src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/js/lightgallery-all.min.js"></script>
		<script type="text/javascript">
			var site_url = '<?= site_url() ?>';
		</script>
		<script type="text/javascript" src="<?= base_url('assets/js/script.js') ?>"></script>
	</body>
</html>