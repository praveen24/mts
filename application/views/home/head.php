<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>MTS - Modern Transport Service Est.</title>
		<link rel="canonical" href="">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="keywords" content="">
                <meta name="description" content="Site Description">
                <link rel="author" href="">
                <link rel="publisher" href="">
                <link rel="icon" href="<?= base_url('assets/img/favicon.png') ?>" type="image/gif" sizes="16x16">
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link href="https://fonts.googleapis.com/css?family=Lato:100i,300,300i,400,700i" rel="stylesheet">
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css">
                <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css') ?>">
                <link href="https://fonts.googleapis.com/css?family=Lemon" rel="stylesheet">
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/css/lightgallery.min.css">
                <link href="https://fonts.googleapis.com/css?family=Cairo:400,600&amp;subset=arabic" rel="  stylesheet">
	</head>
	<body class="<?= $this->session->userdata('arabic') ? 'arabic' : '' ?>">
