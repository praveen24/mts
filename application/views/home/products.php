<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_our_products : strtoupper($menu->en_our_products) ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  Products</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>
<div class="container">
	<div class="row <?= $this->session->userdata('arabic') ? 'arabic_product' : '' ?>" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
		<div id="products">
			<?php foreach ($products as $product) { ?>
				<a href="<?= $product->link ? $product->link : '' ?>" target="_blank">
					<div class="col-lg-6 col-md-6 col-sm-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
						<img src="<?= base_url('uploads/products/'.$product->image) ?>" class="img-fluid" style="min-height: 220px;max-height: 300px;padding-bottom: 10px;">
						<h4 style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>"><?= $this->session->userdata('arabic') ? $product->ar_title : $product->en_title ?></h4>
						<?= $this->session->userdata('arabic') ? $product->ar_desc : $product->en_desc ?>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</div>
<div class="clearfix"> </div><br><br>