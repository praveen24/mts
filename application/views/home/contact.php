<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  Contact Us</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>
<div class="container">
	<div class="row contact <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
		<div class="col-12">
			<div class="title" style="margin-bottom: 30px;<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
				<h2><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></h2>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-12" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
			<h5 style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?=  $this->session->userdata('arabic') ? 'عنوان' : 'ADDRESS' ?></h5>
			<p>Lorem Ipsum dolor sit amet</p>
			<p>Saudi Arabia</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-12" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
			<h5 style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>"><i class="fa fa-phone"></i>&nbsp;&nbsp;<?= $this->session->userdata('arabic') ? 'هاتف' : 'PHONE' ?></h5>
			<p><?= $this->session->userdata('arabic') ? '+٩٦٦١٢٦٢٣١٦٦٦' : '+966126231666' ?></p>
			<h5 style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?= $this->session->userdata('arabic') ? 'البريد الالكتروني' : 'EMAIL' ?></h5>
			<p>domain@gmail.com</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-12" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
			<h5 style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?= $this->session->userdata('arabic') ? 'تابعنا' : 'FOLLOW US' ?></h5>
			<ul class="contact-social">
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
			</ul>
		</div>
	</div>
</div>
<?php if ($this->session->flashdata('success')): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo $this->session->flashdata('success'); ?>
	</div>
<?php endif; ?>
<div class="clearfix"></div><br><br>
<div class="container-fluid">
	<div class="row message-section">
		<div class="container">
			<div class="row <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>">
				<div class="col-12">
					<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
						<h2><?= $this->session->userdata('arabic') ? 'رسالة لنا' : 'MESSAGE US' ?></h2>
					</div>
					<div class="clearfix"></div><br>
					<form action="<?= site_url('home/sendmail') ?>" method="post">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-12">
								<div class="form-group">
									<input type="text" name="name" class="form-control" placeholder="<?= $this->session->userdata('arabic') ? 'الاسم*' : 'Name*' ?>" style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-12">
								<div class="form-group">
									<input type="email" name="email" class="form-control" placeholder="<?= $this->session->userdata('arabic') ? 'البريد الالكتروني*' : 'Email*' ?>" style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-12">
								<div class="form-group">
									<input type="text" name="phone" class="form-control" placeholder="<?= $this->session->userdata('arabic') ? 'لهاتف*' : 'Phone*' ?>" style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>">
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group">
									<textarea class="form-control" placeholder="<?= $this->session->userdata('arabic') ? 'الرسالة' : 'Message' ?>" rows="10" style="height: auto;<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>" name="message"></textarea>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group text-center">
									<button class="btn btn-send"><?= $this->session->userdata('arabic') ? 'إرسال' : 'SEND' ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 nopadding">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14950961.017125897!2d36.06563849996524!3d23.813728067290537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15e7b33fe7952a41%3A0x5960504bc21ab69b!2sSaudi+Arabia!5e0!3m2!1sen!2sin!4v1517566090779" width="100%" height="450" frameborder="0" style="border:0;position: relative;top: 5px;" allowfullscreen></iframe>
		</div>
	</div>
</div>