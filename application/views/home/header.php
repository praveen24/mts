<div class="container-fluid">
      <div class="row main-header">
            <div class="container">
                  <div class="row">
            		<div class="col-lg-4 col-md-4 col-sm-8 col-10">
            			<a href="<?= site_url() ?>"><img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid"></a>
            		</div>
            		<div class="col-lg-8 col-md-8 col-sm-4 col-2">
            			<nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light pull-right">
            			      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar10" onclick="openNav()">
            	            	    <span class="navbar-toggler-icon"></span>
            	        	      </button>
            	        	      <div class="collapse navbar-collapse row no-gutters" id="navbar10">
                				      <ul class="navbar-nav" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
                  				      <li class="nav-item <?= $this->uri->segment(1) == '' ? 'active' : '' ?>">
                  					      <a href="<?= site_url() ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a>
                  				      </li>
                  				      <li class="nav-item <?= $this->uri->segment(1) == 'aboutus' ? 'active' : '' ?>">
                  					      <a href="<?= site_url('aboutus') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a>
                  				      </li>
                  				      <li class="nav-item <?= $this->uri->segment(1) == 'products' ? 'active' : '' ?>">
                  					      <a href="<?= site_url('products') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_products : $menu->en_products ?></a>
                  				      </li>
                  				      <li class="nav-item <?= $this->uri->segment(1) == 'services' ? 'active' : '' ?>">
                  					      <a href="<?= site_url('services') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_services : $menu->en_services ?></a>
                  				      </li>
                                                <li class="nav-item <?= $this->uri->segment(1) == 'gallery' ? 'active' : '' ?>">
                                                      <a href="<?= site_url('gallery') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></a>
                                                </li>
                  				      <li class="nav-item <?= $this->uri->segment(1) == 'contact' ? 'active' : '' ?>">
                  					      <a href="<?= site_url('contact') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></a>
                  				      </li>
                                                <li class="nav-item">
                                                      <a href="#" class="nav-link lang_switch" data-url="<?= current_url() ?>" data-id="<?= $this->session->userdata('arabic') ? 'english' : 'arabic' ?>"><?= $this->session->userdata('arabic') ? 'English' : 'عربى' ?></a>
                                                </li>
                  			      </ul>
                  		      </div>
                                    <div id="mySidenav" class="sidenav" style="display: block; width: 0px; left: 0px;">
                                          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                                          <a href="<?= site_url() ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a>
                                          <a href="<?= site_url('aboutus') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a>
                                          <a href="<?= site_url('products') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_products : $menu->en_products ?></a>
                                          <a href="<?= site_url('services') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_services : $menu->en_services ?></a>
                                          <a href="<?= site_url('gallery') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></a>
                                          <a href="<?= site_url('contact') ?>" class="nav-link"><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></a>
                                          <a href="#" class="nav-link lang_switch" data-url="<?= current_url() ?>" data-id="<?= $this->session->userdata('arabic') ? 'english' : 'arabic' ?>"><?= $this->session->userdata('arabic') ? 'English' : 'عربى' ?></a>
                                      </div>
            		      </nav>
            	      </div>
                  </div>
            </div>
	</div>
</div>