<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_our_services : strtoupper($menu->en_our_services) ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  Services</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>
<div class="container">
	<div class="row service" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
        <?php foreach ($services as $service) { ?>
            <div class="col-sm-12 col-md-4 col-lg-4 col-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
                <div class="single-service">
                    <div class="service-img">
                        <img src="<?= base_url('uploads/services/'.$service->image) ?>" class="img-fluid">
                    </div>
                    <div class="service-content">
                        <h4 style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>"><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h4>
                        <p><?= $this->session->userdata('arabic') ? (mb_strlen(strip_tags($service->ar_desc)) <= 100 ? strip_tags($service->ar_desc) : mb_substr(strip_tags($service->ar_desc), 0, 100).'...') : (strlen(strip_tags($service->en_desc)) <= 100 ? strip_tags($service->en_desc) : substr(strip_tags($service->en_desc), 0, 100).'...') ?></p>
                        <div class="read_more" style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>">
                            <a href="<?= site_url('service/'.$service->id) ?>"><?= $this->session->userdata('arabic') ? 'اقرأ أكثر' : 'READ MORE' ?> <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div class="clearfix"></div><br><br>