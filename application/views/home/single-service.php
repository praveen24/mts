<div class="container-fluid">
	<div class="row about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h1>
				</div>
				<div class="col-12">
					<a href="<?= site_url() ?>">Home&nbsp;</a> <span>  /  <a href="<?= site_url('services') ?>">Services</a> / <?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>
<div class="container">
	<div class="row" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse' : '' ?>">
		<div class="col-lg-8 col-md-8 col-sm-12 col-12 <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
			<div class="single_service_details">
				<div class="single_service_img">
					<img src="<?= base_url('uploads/services/'.$service->image) ?>" class="img-fluid">
				</div>
				<div class="single_service">
					<div class="title" style="<?= $this->session->userdata('arabic') ? 'float: right' : '' ?>">
						<h3><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h3>
					</div>
					<div class="clearfix"></div><br>
					<?= $this->session->userdata('arabic') ? $service->ar_desc : $service->en_desc ?>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-12">
			<div class="more_services">
				<ul>
					<?php foreach ($other_services as $key => $service) { ?>
						<li style="<?= $this->session->userdata('arabic') ? 'text-align: right' : '' ?>"><a href="<?= site_url('service/'.$service->id) ?>"><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br><br>