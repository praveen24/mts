<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> Add Video Gallery
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <form method="POST" action="<?= site_url('admin/add_video_gallery') ?>" enctype="multipart/form-data">
                <div class="form-group <?= form_error('link') ? 'has-error' : '' ?>">
                    <label> Link </label>
                    <input type="text" name="link" id="link" class="form-control" placeholder="Enter the Link here" value="<?= set_value('link') ?>">
                    <span class="<?= form_error('link') ? 'text-danger' : '' ?>"><?= form_error('link') ?></span>
                </div>
                <div class="clearfix"></div><br>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>