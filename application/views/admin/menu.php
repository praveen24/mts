<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> Manage Menu
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <form method="POST" action="<?= site_url('admin/menu') ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2 text-center">
                        <label>English</label>
                    </div>
                    <div class="col-md-5 text-center">
                        <label>Arabic</label>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="row">
                    <div class="col-md-2">
                        <label> Home </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_home') ? 'has-error' : '' ?>">
                        <input type="text" name="en_home" id="en_home" class="form-control" placeholder="Enter the English Home here" value="<?= $menu->en_home ?>">
                        <span class="<?= form_error('en_home') ? 'text-danger' : '' ?>"><?= form_error('en_home') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_home') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_home" dir="rtl" id="ar_home" class="form-control" placeholder="Enter the Arabic Home here" value="<?= $menu->ar_home ?>">
                        <span class="<?= form_error('ar_home') ? 'text-danger' : '' ?>"><?= form_error('ar_home') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> About </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_about') ? 'has-error' : '' ?>">
                        <input type="text" name="en_about" id="en_about" class="form-control" placeholder="Enter the English -About here" value="<?= $menu->en_about ?>">
                        <span class="<?= form_error('en_about') ? 'text-danger' : '' ?>"><?= form_error('en_about') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_about') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_about" dir="rtl" id="ar_about" class="form-control" placeholder="Enter the Arabic -About here" value="<?= $menu->ar_about ?>">
                        <span class="<?= form_error('ar_about') ? 'text-danger' : '' ?>"><?= form_error('ar_about') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Products </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_products') ? 'has-error' : '' ?>">
                        <input type="text" name="en_products" id="en_products" class="form-control" placeholder="Enter the English - Products here" value="<?= $menu->en_products ?>">
                        <span class="<?= form_error('en_products') ? 'text-danger' : '' ?>"><?= form_error('en_products') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_products') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_products" dir="rtl" id="ar_products" class="form-control" placeholder="Enter the Arabic - Products here" value="<?= $menu->ar_products ?>">
                        <span class="<?= form_error('ar_products') ? 'text-danger' : '' ?>"><?= form_error('ar_products') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Services </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_services') ? 'has-error' : '' ?>">
                        <input type="text" name="en_services" id="en_services" class="form-control" placeholder="Enter the English - Services here" value="<?= $menu->en_services ?>">
                        <span class="<?= form_error('en_services') ? 'text-danger' : '' ?>"><?= form_error('en_services') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_services') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_services" dir="rtl" id="ar_services" class="form-control" placeholder="Enter the Arabic - Services here" value="<?= $menu->ar_services ?>">
                        <span class="<?= form_error('ar_services') ? 'text-danger' : '' ?>"><?= form_error('ar_services') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Contact </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_contact') ? 'has-error' : '' ?>">
                        <input type="text" name="en_contact" id="en_contact" class="form-control" placeholder="Enter the English -Contact here" value="<?= $menu->en_contact ?>">
                        <span class="<?= form_error('en_contact') ? 'text-danger' : '' ?>"><?= form_error('en_contact') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_contact') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_contact" dir="rtl" id="ar_contact" class="form-control" placeholder="Enter the Arabic -Contact here" value="<?= $menu->ar_contact ?>">
                        <span class="<?= form_error('ar_contact') ? 'text-danger' : '' ?>"><?= form_error('ar_contact') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Spare Parts </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_spare') ? 'has-error' : '' ?>">
                        <input type="text" name="en_spare" id="en_spare" class="form-control" placeholder="Enter the English -Spare Parts here" value="<?= $menu->en_spare ?>">
                        <span class="<?= form_error('en_spare') ? 'text-danger' : '' ?>"><?= form_error('en_spare') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_spare') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_spare" dir="rtl" id="ar_spare" class="form-control" placeholder="Enter the Arabic -Spare Parts here" value="<?= $menu->ar_spare ?>">
                        <span class="<?= form_error('ar_spare') ? 'text-danger' : '' ?>"><?= form_error('ar_spare') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Support </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_support') ? 'has-error' : '' ?>">
                        <input type="text" name="en_support" id="en_support" class="form-control" placeholder="Enter the English -Support here" value="<?= $menu->en_support ?>">
                        <span class="<?= form_error('en_support') ? 'text-danger' : '' ?>"><?= form_error('en_support') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_support') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_support" dir="rtl" id="ar_support" class="form-control" placeholder="Enter the Arabic -Support here" value="<?= $menu->ar_support ?>">
                        <span class="<?= form_error('ar_support') ? 'text-danger' : '' ?>"><?= form_error('ar_support') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Our Services </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_our_services') ? 'has-error' : '' ?>">
                        <input type="text" name="en_our_services" id="en_our_services" class="form-control" placeholder="Enter the English -Our Services here" value="<?= $menu->en_our_services ?>">
                        <span class="<?= form_error('en_our_services') ? 'text-danger' : '' ?>"><?= form_error('en_our_services') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_our_services') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_our_services" dir="rtl" id="ar_our_services" class="form-control" placeholder="Enter the Arabic -Our Services here" value="<?= $menu->ar_our_services ?>">
                        <span class="<?= form_error('ar_our_services') ? 'text-danger' : '' ?>"><?= form_error('ar_our_services') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Our Products </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_our_products') ? 'has-error' : '' ?>">
                        <input type="text" name="en_our_products" id="en_our_products" class="form-control" placeholder="Enter the English -Our Products here" value="<?= $menu->en_our_products ?>">
                        <span class="<?= form_error('en_our_products') ? 'text-danger' : '' ?>"><?= form_error('en_our_products') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_our_products') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_our_products" dir="rtl" id="ar_our_products" class="form-control" placeholder="Enter the Arabic -Our Products here" value="<?= $menu->ar_our_products ?>">
                        <span class="<?= form_error('ar_our_products') ? 'text-danger' : '' ?>"><?= form_error('ar_our_products') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Gallery </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_gallery') ? 'has-error' : '' ?>">
                        <input type="text" name="en_gallery" id="en_gallery" class="form-control" placeholder="Enter the English -Gallery here" value="<?= $menu->en_gallery ?>">
                        <span class="<?= form_error('en_gallery') ? 'text-danger' : '' ?>"><?= form_error('en_gallery') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_gallery') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_gallery" dir="rtl" id="ar_gallery" class="form-control" placeholder="Enter the Arabic -Gallery here" value="<?= $menu->ar_gallery ?>">
                        <span class="<?= form_error('ar_gallery') ? 'text-danger' : '' ?>"><?= form_error('ar_gallery') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>