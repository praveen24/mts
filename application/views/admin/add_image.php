<section id="main-content">
    <section class="wrapper">

        <?php $this->load->view('layouts/notification'); ?>
        <div class="panel  panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"> Add Image
                    <div class="clearfix">  </div>
                </h2>
            </div>
            <!-- /.box-header -->

            <div class="panel-body">
                <form method="POST" action="<?= site_url('admin/add_image_gallery') ?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-2 text-center">
                            <label>English</label>
                        </div>
                        <div class="col-md-5 text-center">
                            <label>Arabic</label>
                        </div>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="row">
                        <div class="col-md-2">
                            <label> Title </label>
                        </div>
                        <div class="col-md-5 form-group <?= form_error('en_title') ? 'has-error' : '' ?>">
                            <input type="text" name="en_title" id="en_title" class="form-control" placeholder="Enter the English Title here" value="<?= set_value('en_title') ?>">
                            <span class="<?= form_error('en_title') ? 'text-danger' : '' ?>"><?= form_error('en_title') ?></span>
                        </div>
                        <div class="col-md-5 form-group <?= form_error('ar_title') ? 'has-error' : '' ?>">
                            <input type="text" dir="rtl" name="ar_title" id="ar_title" class="form-control" placeholder="Enter the Arabic Title here" value="<?= set_value('ar_title') ?>">
                            <span class="<?= form_error('ar_title') ? 'text-danger' : '' ?>"><?= form_error('ar_title') ?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label> Image (preferred size 360*210) </label>
                        </div>
                        <div class="col-sm-10 <?= form_error('uploadImage') ? 'has-error' : '' ?>">
                            <input type="file" name="uploadfile">
                        </div>
                    </div>
                    <div class="clearfix"></div><br>
                    
                    <button type="submit" class="btn btn-primary" id="submit2" >Submit</button>
                    <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
</section>
