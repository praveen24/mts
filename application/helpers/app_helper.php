<?php 
	if(!function_exists('is_video')){
		function is_video($file){
			return in_array(strtolower(ltrim(strrchr($file, '.'), '.')), ['mp4', 'mpeg', 'wmv', 'ogg', 'ogv', 'webm']);
		}
	}