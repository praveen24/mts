<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('admin_model');
        $about = $this->home_model->getAll('about_us');
		$this->data['about_us'] = isset($about[0])?$about[0]:false;
		$this->data['menu'] = $this->home_model->getOneWhere(array('status' => 1), 'menu');
    }


	public function index()
	{
		$this->data['sliders'] = $this->home_model->getAll('sliders');
		
		$this->db->limit(3);
		$this->data['highlight_services'] = $this->home_model->getWhere(array('highlight' => 1, 'status' => 1), 'services');
		$this->db->limit(4);
		$this->data['highlight_products'] = $this->home_model->getWhere(array('highlight' => 1, 'status' => 1), 'products');
		$this->data['view_page'] = 'home/index';
        $this->load->view('home/template', $this->data);
	}

	public function about()
	{
		$this->data['view_page'] = 'home/about';
        $this->load->view('home/template', $this->data);
	}

	public function contact()
	{
		$this->data['view_page'] = 'home/contact';
        $this->load->view('home/template', $this->data);
	}

	public function services()
	{
		$this->data['services'] = $this->home_model->getWhere(array('status' => 1), 'services');
		if(count($this->data['services']) == 0)
			redirect('home');
		$this->data['view_page'] = 'home/services';
        $this->load->view('home/template', $this->data);
	}

	public function service($id)
	{
		$this->data['service'] = $this->home_model->getOneWhere(array("id" => $id), 'services');
		$this->data['other_services'] = $this->home_model->getWhere(array("id !=" => $id), 'services');
		$this->data['view_page'] = 'home/single-service';
        $this->load->view('home/template', $this->data);
	}

	public function products()
	{
		$this->data['products'] = $this->home_model->getWhere(array('status' => 1), 'products');
		if(count($this->data['products']) == 0)
			redirect('home');
		$this->data['view_page'] = 'home/products';
        $this->load->view('home/template', $this->data);
	}
	public function gallery()
	{
		$this->data['images'] = $this->home_model->getWhere(array('status' => 1), 'image_gallery');
		$this->data['gallery'] = $this->home_model->getAll('video_gallery');
		$this->data['view_page'] = 'home/gallery';
        $this->load->view('home/template', $this->data);
	}

	public function manage_language(){
		if ($this->input->post('language') == 'arabic')
			$this->session->set_userdata('arabic', 1);
		else
			$this->session->unset_userdata('arabic');
		echo "true";
	}

	public function sendmail()
	{
	    $name = $this->input->post('name'); 
	    $email = $this->input->post('email'); 
	    $phone = $this->input->post('phone'); 
	    $message = $this->input->post('message');

	    $data = array('name' => $name, 'email' => $email, 'phone' => $phone, 'message' =>$message);

	    $mail_body = $this->load->view('mail/contactus', $data, TRUE);

	    $this->load->library('email');

		$this->email->from($email, $name);
		$this->email->to('fasil@soarmorrow.com');

		$this->email->subject('MTS Contact Form');
		$this->email->message($mail_body);

		$this->email->send(false);

		$this->session->set_flashdata('success', 'Thank You for Contacting us. We will get back to you Soon');
		redirect('contact');
	}
	public function iframe(){
		$this->load->view('home/iframe', ['file' => $this->input->get('file')]); 
	}
}
