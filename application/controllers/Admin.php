<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once('application/core/AdminController.php');

class Admin extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('upload');
    }

    public function index() {
        redirect(site_url('auth/login'));
    }

    public function home() {
        $data['view_page'] = 'index.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function change_password() {
        $this->data['user'] = $this->admin_model->getOneWhere(array('id' => $this->session->userdata('user_id')), 'user');
        if ($this->input->post()) {
            if (password_verify($this->input->post('current_password'), $this->data['user']->password)) {
                $this->checkValidationChangePass();
                $this->data['passworderror'] = false;
            } else {
                $this->data['passworderror'] = true;
            }
        }
        $data['view_page'] = 'change_password';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function checkValidationChangePass() {
        $this->form_validation->set_rules('password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');
        if ($this->form_validation->run($this) == true) {
            if ($this->gr_auth->reset_password($this->session->userdata('user_id'), $this->input->post('password'))) {
                $this->session->set_flashdata('success', 'Password Updated Successfully');
                redirect(site_url('admin/home'));
            }
        }
    }

    public function sliders() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/sliders');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadSliders(0, 0, TRUE, $content);

        $this->data['sliders'] = $this->admin_model->loadSliders($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/sliders.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_slider() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'English Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {
                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/sliders/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png|mp4|mpeg|wmv|ogg|ogv|webm';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_slider');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'image' => $image
                    );
                    $this->admin_model->insertRow($data, 'sliders');

                    $this->session->set_flashdata('success', 'Slider added Successfully');
                    redirect('admin/sliders');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_slider');
                }
            }
        }
        $data['view_page'] = 'admin/add_slider';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_slider($id) {

        $get_slider = $this->admin_model->getOneWhere(array('id' => $id), 'sliders');
        if (!$get_slider) {
            redirect(site_url());
        } else {
            @unlink('uploads/sliders/' . $get_slider->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'sliders');
            $this->session->set_flashdata('success', 'Slider Deleted Successfully');
            redirect(site_url('admin/sliders'));
        }
    }

    public function menu() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_home', 'English Home', 'required');
            $this->form_validation->set_rules('en_about', 'English About', 'required');
            $this->form_validation->set_rules('en_products', 'English Products', 'required');
            $this->form_validation->set_rules('en_services', 'English Services', 'required');
            $this->form_validation->set_rules('en_contact', 'English Contact', 'required');
            $this->form_validation->set_rules('en_spare', 'English Spare Parts', 'required');
            $this->form_validation->set_rules('en_support', 'English Support', 'required');
            $this->form_validation->set_rules('ar_home', 'Arabic Home', 'required');
            $this->form_validation->set_rules('ar_about', 'Arabic About', 'required');
            $this->form_validation->set_rules('ar_products', 'Arabic Products', 'required');
            $this->form_validation->set_rules('ar_services', 'Arabic Services', 'required');
            $this->form_validation->set_rules('ar_contact', 'Arabic Contact', 'required');
            $this->form_validation->set_rules('ar_spare', 'Arabic Spare Parts', 'required');
            $this->form_validation->set_rules('ar_support', 'Arabic Support', 'required');
            $this->form_validation->set_rules('en_our_services', 'English Our Services', 'required');
            $this->form_validation->set_rules('en_our_products', 'English Our Products', 'required');
            $this->form_validation->set_rules('en_gallery', 'English Gallery', 'required');
            $this->form_validation->set_rules('ar_our_services', 'Arabic Our Services', 'required');
            $this->form_validation->set_rules('ar_our_products', 'Arabic Our Products', 'required');
            $this->form_validation->set_rules('ar_gallery', 'Arabic Gallery', 'required');
            if ($this->form_validation->run() == true) {
                $data = array(
                    'en_home' => $this->input->post('en_home'),
                    'en_about' => $this->input->post('en_about'),
                    'en_products' => $this->input->post('en_products'),
                    'en_services' => $this->input->post('en_services'),
                    'en_contact' => $this->input->post('en_contact'),
                    'ar_home' => $this->input->post('ar_home'),
                    'ar_about' => $this->input->post('ar_about'),
                    'ar_products' => $this->input->post('ar_products'),
                    'ar_services' => $this->input->post('ar_services'),
                    'ar_contact' => $this->input->post('ar_contact'),
                    'en_spare' => $this->input->post('en_spare'),
                    'en_support' => $this->input->post('en_support'),
                    'ar_spare' => $this->input->post('ar_spare'),
                    'ar_support' => $this->input->post('ar_support'),
                    'en_our_services' => $this->input->post('en_our_services'),
                    'en_our_products' => $this->input->post('en_our_products'),
                    'ar_our_services' => $this->input->post('ar_our_services'),
                    'ar_our_products' => $this->input->post('ar_our_products'),
                    'en_gallery' => $this->input->post('en_gallery'),
                    'ar_gallery' => $this->input->post('ar_gallery')
                );
                $this->admin_model->updateWhere(array('status' => 1), $data, 'menu');
                $this->session->set_flashdata('success', 'Menu Contents Updated Successfully');
                redirect(site_url('admin/menu'));
            }
            else{
                debug(validation_errors());
            }
        }

        $this->data['menu'] = $this->admin_model->getOneWhere(array('status' => 1), 'menu');

        $data['view_page'] = 'admin/menu.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function about_us() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'required');
            $this->form_validation->set_rules('en_desc', 'Description', 'required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'required');
            if ($this->form_validation->run() == true) {

                $data = array(
                    'en_title' => $this->input->post('en_title'),
                    'ar_title' => $this->input->post('ar_title'),
                    'en_desc' => $this->input->post('en_desc'),
                    'ar_desc' => $this->input->post('ar_desc'),
                );

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/about_us/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/about_us');
                    } else {
                        $data['image'] = $this->upload->data('file_name');
                    }
                }
                $current_about = $this->admin_model->getOneWhere(array('status' => 1), 'about_us');
                @unlink('uploads/about_us/' . $current_about->image);
                $this->admin_model->updateWhere(array('status' => 1), $data, 'about_us');

                $this->session->set_flashdata('success', 'About Us Updated Successfully');
                redirect(site_url('admin/about_us'));
            }
        }

        $aboutUs = $this->admin_model->getAll('about_us');
        if(isset($aboutUs[0]))
            $this->data['about_us'] = $aboutUs[0];
        else
            $this->data['about_us'] = NULL;
        $data['view_page'] = 'admin/about_us.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function upload_files($folder = 'services') {
        if (empty($_FILES['file']['name'])) {

        } else if ($_FILES['file']['error'] == 0) {
            $filetype = NULL;
            //upload and update the file
            $config['upload_path'] = './uploads/' . $folder . '/';
            $config['max_size'] = '102428800';
            $type = $_FILES['file']['type'];
            switch ($type) {
                case 'image/gif':
                case 'image/jpg':
                case 'image/png':
                case 'image/jpeg': {
                    $filetype = 0;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    break;
                }
            }
            $config['overwrite'] = false;
            $config['remove_spaces'] = true;
            if (!file_exists('./uploads/' . $folder)) {
                if (!mkdir('./uploads/' . $folder . '/', 0755, TRUE)) {

                }
            }
            $microtime = microtime(TRUE) * 10000;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file', $microtime)) {
                echo json_encode(array('type' => $filetype, 'path' => 'uploads/' . $folder . '/' . $this->upload->file_name));
            }
        }
        exit;
    }

    public function services() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/services/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadServices(0, 0, TRUE, $content);

        $this->data['services'] = $this->admin_model->loadServices($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/services';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_service() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/services/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_service');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'en_desc' => $this->input->post('en_desc'),
                        'ar_desc' => $this->input->post('ar_desc'),
                        'image' => $image
                    );
                    $service = $this->admin_model->insertRow($data, 'services');

                    $this->session->set_flashdata('success', 'Service added Successfully');
                    redirect('admin/services');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_service');
                }
            }
        }

        $data['view_page'] = 'admin/add_service';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_service($id) {

        $service = $this->admin_model->getOneWhere(array('id' => $id), 'services');
        if (!$service) {
            redirect('admin/services');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/services/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/services');
                    } else {
                        unlink('uploads/services/' . $service->image);
                        $image = $this->upload->data('file_name');
                    }
                } else {
                    $image = $service->image;
                }
                $data = array(
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $service->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $service->ar_title,
                    'en_desc' => $this->input->post('en_desc') ? $this->input->post('en_desc') : $service->en_desc,
                    'ar_desc' => $this->input->post('ar_desc') ? $this->input->post('ar_desc') : $service->ar_desc,
                    'image' => $image
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'services');

                $this->session->set_flashdata('success', 'Service updated Successfully');
                redirect('admin/services');
            }
        }

        $this->data['service'] = $service;

        $data['view_page'] = 'admin/edit_service';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_service($id) {
        $service = $this->admin_model->getOneWhere(array('id' => $id), 'services');
        if (!$service) {
            redirect(site_url());
        } else {
            unlink('uploads/services/' . $service->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'services');

            $this->session->set_flashdata('success', 'Service Deleted Successfully');
            redirect(site_url('admin/services'));
        }
    }

    public function products() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/products/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadProducts(0, 0, TRUE, $content);

        $this->data['products'] = $this->admin_model->loadProducts($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/products';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_product() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');
            $this->form_validation->set_rules('link', 'Website Link', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/products/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_product');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'en_desc' => $this->input->post('en_desc'),
                        'ar_desc' => $this->input->post('ar_desc'),
                        'link' => $this->input->post('link'),
                        'image' => $image
                    );
                    $product = $this->admin_model->insertRow($data, 'products');

                    $this->session->set_flashdata('success', 'Product added Successfully');
                    redirect('admin/products');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_product');
                }
            }
        }

        $data['view_page'] = 'admin/add_product';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_product($id) {

        $product = $this->admin_model->getOneWhere(array('id' => $id), 'products');
        if (!$product) {
            redirect('admin/products');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');
            $this->form_validation->set_rules('link', 'Website Link', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/products/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/products');
                    } else {
                        unlink('uploads/products/' . $product->image);
                        $image = $this->upload->data('file_name');
                    }
                } else {
                    $image = $product->image;
                }
                $data = array(
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $product->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $product->ar_title,
                    'en_desc' => $this->input->post('en_desc') ? $this->input->post('en_desc') : $product->en_desc,
                    'ar_desc' => $this->input->post('ar_desc') ? $this->input->post('ar_desc') : $product->ar_desc,
                    'link' => $this->input->post('link') ? $this->input->post('link') : $product->link,
                    'image' => $image
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'products');

                $this->session->set_flashdata('success', 'Product updated Successfully');
                redirect('admin/products');
            }
        }

        $this->data['product'] = $product;

        $data['view_page'] = 'admin/edit_product';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_product($id) {
        $product = $this->admin_model->getOneWhere(array('id' => $id), 'products');
        if (!$product) {
            redirect(site_url());
        } else {
            unlink('uploads/products/' . $product->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'products');

            $this->session->set_flashdata('success', 'Product Deleted Successfully');
            redirect(site_url('admin/products'));
        }
    }





    public function image_gallery() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/image_gallery/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadImage(0, 0, TRUE, $content);

        $this->data['image_gallery'] = $this->admin_model->loadImage($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/image_gallery';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_image_gallery() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/image/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_product');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'image' => $image
                    );
                    $image = $this->admin_model->insertRow($data, 'image_gallery');

                    $this->session->set_flashdata('success', 'Image added Successfully');
                    redirect('admin/image_gallery');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_image_gallery');
                }
            }
        }

        $data['view_page'] = 'admin/add_image';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_image_gallery($id) {

        $image = $this->admin_model->getOneWhere(array('id' => $id), 'image_gallery');
        if (!$image) {
            redirect('admin/image_gallery');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/image/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/image_gallery');
                    } else {
                        unlink('uploads/image/' . $image->image);
                        $image = $this->upload->data('file_name');
                    }
                } else {
                    $image = $image->image;
                }
                $data = array(
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $image->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $image->ar_title,
                    'image' => $image
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'image_gallery');

                $this->session->set_flashdata('success', 'Image updated Successfully');
                redirect('admin/image_gallery');
            }
        }

        $this->data['image'] = $image;

        $data['view_page'] = 'admin/edit_image';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_image_gallery($id) {
        $image = $this->admin_model->getOneWhere(array('id' => $id), 'image_gallery');
        if (!$image) {
            redirect(site_url());
        } else {
            unlink('uploads/products/' . $image->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'image_gallery');

            $this->session->set_flashdata('success', 'Image Deleted Successfully');
            redirect(site_url('admin/image_gallery'));
        }
    }


    public function video_gallery() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/video_gallery');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadVideoGallery(0, 0, TRUE, $content);

        $this->data['video_gallery'] = $this->admin_model->loadVideoGallery($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/video_gallery.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_video_gallery() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('link', 'Link', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                $data = array(
                    'link' => $this->input->post('link')
                    );
                $this->admin_model->insertRow($data, 'video_gallery');

                $this->session->set_flashdata('success', 'Video Gallery added Successfully');
                redirect('admin/video_gallery');
            }
        }
        $data['view_page'] = 'admin/add_video_gallery';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_video_gallery($id) {

        $get_video_gallery = $this->admin_model->getOneWhere(array('id' => $id), 'video_gallery');
        if (!$get_video_gallery) {
            redirect(site_url());
        } else {
            $this->admin_model->deleteWhere(array('id' => $id), 'video_gallery');
            $this->session->set_flashdata('success', 'Video Gallery Deleted Successfully');
            redirect(site_url('admin/video_gallery'));
        }
    }

    public function manage_highlight() {
        $id = $this->input->post('id');
        $table = $this->input->post('table');
        $this->admin_model->updateWhere(array('id' => $id), array('highlight' => $this->input->post('val')), ($table?$table:'services'));
        echo true;
    }

}
