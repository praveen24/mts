<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin model
 *
 * @author Kunhu
 */
class Admin_model extends MY_Model {

    //put your code here
	public function __construct($table_name = NULL, $primary_key = NULL) {
		parent::__construct($table_name, $primary_key);
	}


    public function loadSliders($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('sliders');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('en_title', trim($content));
            $this->db->or_like('ar_title', trim($content));
            $this->db->or_like('en_title_large', trim($content));
            $this->db->or_like('ar_title_large', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }
    public function loadContacts($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('contact');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('name', trim($content));
            $this->db->or_like('email', trim($content));
            $this->db->or_like('phone', trim($content));
            $this->db->or_like('message', trim($content));
            $this->db->group_end();
        }
        $this->db->where('status', 1);
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }
    public function loadReservations($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('reservation');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('name', trim($content));
            $this->db->or_like('email', trim($content));
            $this->db->or_like('phone', trim($content));
            $this->db->or_like('section', trim($content));
            $this->db->group_end();
        }
        $this->db->where('status', 1);
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadGallery($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('gallery');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('en_title', trim($content));
            $this->db->or_like('ar_title', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadEventGallery($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('event_gallery');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('en_title', trim($content));
            $this->db->or_like('ar_title', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadVideoGallery($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('video_gallery');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('link', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }


    public function loadAboutUsHistory($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('about_us_history');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('p.en_year', trim($content));
            $this->db->or_like('p.ar_year', trim($content));
            $this->db->or_like('p.en_title', trim($content));
            $this->db->or_like('p.ar_title', trim($content));
            $this->db->or_like('p.en_desc', trim($content));
            $this->db->or_like('p.ar_desc', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadProducts($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('products');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('p.en_title', trim($content));
            $this->db->or_like('p.ar_title', trim($content));
            $this->db->or_like('p.en_desc', trim($content));
            $this->db->or_like('p.ar_desc', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadImage($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('image_gallery');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('p.en_title', trim($content));
            $this->db->or_like('p.ar_title', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadServices($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('services');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('p.en_title', trim($content));
            $this->db->or_like('p.ar_title', trim($content));
            $this->db->or_like('p.en_desc', trim($content));
            $this->db->or_like('p.ar_desc', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

    public function loadEvents($perpage, $page, $count, $content = "") {
        $data = array();
        $this->db->select('*')
                ->from('events');

        if ($content != NULL) {
            $this->db->group_start();
            $this->db->or_like('p.en_name', trim($content));
            $this->db->or_like('p.ar_name', trim($content));
            $this->db->or_like('p.en_title', trim($content));
            $this->db->or_like('p.ar_title', trim($content));
            $this->db->or_like('p.en_desc', trim($content));
            $this->db->or_like('p.ar_desc', trim($content));
            $this->db->group_end();
        }
        $this->db->order_by("id", "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $data = $this->db->get();
        if ($count) {
            return $data->num_rows();
        }
        return $data->result();
    }

}

?>