ALTER TABLE `menu`
  DROP `en_services`,
  DROP `ar_services`,
  DROP `en_events`,
  DROP `ar_events`;

ALTER TABLE `menu` 
	CHANGE `en_reservation` `en_products` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	CHANGE `en_gallery` `en_services` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	CHANGE `ar_reservation` `ar_products` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	CHANGE `ar_gallery` `ar_services` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	ADD `en_spare` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `en_contact`,
	ADD `en_support` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `en_spare`,
	ADD `ar_spare` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `ar_contact`,
	ADD `ar_support` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `ar_spare`,
	ADD `en_our_services` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `en_support`,
	ADD `en_our_products` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `en_our_services`,
	ADD `ar_our_services` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `ar_support`,
	ADD `ar_our_products` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `ar_our_services`;

TRUNCATE menu;

INSERT INTO `menu` (`id`, `en_home`, `en_about`, `en_products`, `en_services`, `en_contact`, `en_spare`, `en_support`, `en_our_services`, `en_our_products`, `ar_home`, `ar_about`, `ar_products`, `ar_services`, `ar_contact`, `ar_spare`, `ar_support`, `ar_our_services`, `ar_our_products`, `status`) VALUES
(1, 'Home', 'About Us', 'Products', 'Services', 'Contact', 'Spare Parts', 'Support', 'Our Services', 'Our Products', ' الصفحة الرئيسية', 'معلومات عنا', 'الحجز', 'خدمات', 'اتصل بنا', 'قطعة منفصلة', 'الدعم', 'خدماتنا', 'منتجاتنا', 1);
