-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 02, 2018 at 03:48 PM
-- Server version: 5.7.20-0ubuntu0.17.04.1
-- PHP Version: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mts`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `en_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_desc` text CHARACTER SET utf8 NOT NULL,
  `ar_desc` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `en_title`, `ar_title`, `en_desc`, `ar_desc`, `image`, `status`) VALUES
(1, 'We provide the most luxurious services', 'تأمين لحظات لا تنسى', '<p>Malekat Palace is a full service banquet and catering company. we pride our selves on offering the perfect venue for creating memorable events, casual or formal, small or large. We specialize in weddings, anniversaries, and every special moments. We know there are many factors that make an event go from good to great and we work hard to deliver just that. Through our friendly staff...</p>\r\n', '<p>في قصر الملكات ، ستخوضون غمار تجربة تحمل بصمة خاصة لبدء رحلة خاصة. توفر قاعات الحفلات الخاصة بنا أجواء أنيقة ومثالية لحفل زفاف حميم وخيارات متنوعة من المساحات المرنة والمواقع المتاحة لضمان أن يكون لديك المكان المثالي ليوم حلمك. فالحدث المثالي يبدأ من الموقع المثالي. ويتيح لكم قصر الملكات&nbsp;الكثير من الأماكن المختلفة لاستيعاب حفلات الزفاف الحميمة والحفلات الخاصة واستقبالات الشركات. إن تناغم ديكوراتنا المتميزة مع المحيط الفخم يوفر أجواء مذهلة لحدث لا ينسى.</p>\r\n', '3390f9031e9255c771e4f6eba46055fb.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `en_home` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_about` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_reservation` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_gallery` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_contact` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_home` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_about` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_reservation` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_gallery` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_contact` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_services` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_services` varchar(200) CHARACTER SET utf8 NOT NULL,
  `en_events` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ar_events` varchar(150) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `en_home`, `en_about`, `en_reservation`, `en_gallery`, `en_contact`, `ar_home`, `ar_about`, `ar_reservation`, `ar_gallery`, `ar_contact`, `en_services`, `ar_services`, `en_events`, `ar_events`, `status`) VALUES
(1, 'Home', 'About', 'Reservation', 'Gallery', 'Contact', ' الصفحة الرئيسية', 'معلومات عنا', 'الحجز', 'المعرض', 'اتصل بنا', 'Services', 'الخدمات', 'Events', 'الأحداث', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `en_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ar_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `en_desc` text CHARACTER SET utf8 NOT NULL,
  `ar_desc` text CHARACTER SET utf8 NOT NULL,
  `highlight` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `en_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ar_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `en_desc` text CHARACTER SET utf8 NOT NULL,
  `ar_desc` text CHARACTER SET utf8 NOT NULL,
  `highlight` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `en_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ar_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `activation_code` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `activation_code`, `status`, `is_admin`, `is_deleted`, `timestamp`) VALUES
(1, 'admin', 'kunhu707@gmail.com', '$2y$10$wa9kLdC30p840bbrE5D2sex.jAEMUV1eZwaAbTIRdpBbarMg9/3Ra', '', 1, 1, 0, '2017-12-13 23:24:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
