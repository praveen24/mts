
function videoFunction() { 
    var vid = document.getElementById("video-list");
      alert(vid.duration);
}
function openNav() {
	document.getElementById("mySidenav").style.display = "block";
	document.getElementById("mySidenav").style.width = "300px";
	document.getElementById("main").style.marginLeft = "300px";
	document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
	document.getElementById("main").style.marginLeft= "0";
	document.body.style.backgroundColor = "white";
}
$(document).ready(function() {
  	// Vimeo API nonsense
  var player = document.getElementById('player_1');
  $f(player).addEvent('ready', ready);
 
  function addEvent(element, eventName, callback) {
    if (element.addEventListener) {
      element.addEventListener(eventName, callback, false)
    } else {
      element.attachEvent(eventName, callback, false);
    }
  }
 
  function ready(player_id) {
    var froogaloop = $f(player_id);
    froogaloop.addEvent('play', function(data) {
      $('.flexslider').flexslider("pause");
      players[0].api('setVolume', 0);
    });
    froogaloop.addEvent('pause', function(data) {
      $('.flexslider').flexslider("play");
    });
  }
 
 
  // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
  $(".flexslider")
    .fitVids()
    .flexslider({
      animation: "fade",
      useCSS: false,
      animationLoop: true,
      smoothHeight: true,
      before: function(slider){
        $f(player).api('pause');
      }
  });
});
$('#gallery,#video_gallery').lightGallery({
    thumbnail:true
});

$(".lang_switch").click(function(e){

	e.preventDefault();
	var language = $(this).data('id');
	var current_url = $(this).data('url');

	$.ajax({
	  type: "POST",
	  url: site_url + 'home/manage_language',
	  data: {language : language, current_url : current_url},
	  success: function (data) {
	  	window.location = current_url;
	  }
	});

});
!function ($) {

    // setTimeout(function(){
    //     if (typeof sliderPage != null){
    //         var duration = 2000;
    //         var vids = document.getElementsByClassName('slider_video');
    //         for (var i=0;i<vids.length;i++){
    //             if (!isNaN(vids[i].duration) && Math.floor(vids[i].duration * 1000) > duration){
    //                 duration = Math.floor(vids[i].duration * 1000);
    //             }
    //         }
    //         console.log(duration)
    //         $('#homepage_slider').carousel({
    //             interval : duration
    //         })
    //     }
    // },2000);

    $(function(){
      $('#homepage_slider').carousel()
    })
}(window.jQuery)